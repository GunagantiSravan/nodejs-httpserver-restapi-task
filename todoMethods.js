const todosData = require("./todo.js")

class TodoMethods {

    async getAllTodos() {
        return new Promise((resolve,reject) => {
            (todosData.length === 0) ? reject("The todos data is empty") : resolve(todosData)
        })
    }


    async getTodo(id) {
        return new Promise((resolve,reject) => {
            const todo = todosData.find(todo=>todo.id === parseInt(id))
            if (todo) {
                resolve(todo)
            } else {
                reject(`Todo with id ${id} is not found`)
            }
        })
    }

    async createNewTodo(todo) {
        return new Promise((resolve,reject) => {
            const newTodo = {
                id : Math.floor(30 + Math.random()*100),
                ...todo
            }
            todosData.push(newTodo)
            resolve(newTodo)
        })
    }


    async updateTodo(id) {
        return new Promise((resolve,reject) => {
            const todo = todosData.find(todo=>todo.id === parseInt(id))
            if (todo) {
                let todostatus=todo["is_complete"];
                todo["is_complete"]=!todostatus;
                resolve(todo)
            } else {
                reject(`Todo with id ${id} is not found`)
            }
        })
    }


    async deleteTodo(id) {
        return new Promise((resolve,reject)=>{
            const todo = todosData.find(todo=>todo.id===parseInt(id))
            if (!todo) {
                reject(`No todo with id ${id} found`) 
            } else {
                resolve(`todo deleted successfully`)
            }
        })
    }

}

module.exports = TodoMethods;