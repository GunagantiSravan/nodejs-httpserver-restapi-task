const http = require('http');
const TodoMethods=require('./todoMethods.js')

module.exports = http.createServer(async(request, response) => {
    if (request.url === "/todos" && request.method ==="GET") {
        try {
            const todos = await new TodoMethods().getAllTodos()
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todos))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message:error}))
        }
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="GET") {
        try {
            const id = request.url.split('/')[2];
            const todo = await new TodoMethods().getTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todo))
            

        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message:error}))
        }
    } else if (request.url==='/todos' && request.method ==="POST") {
        try {
            const todoData = await getReqData(request)
            const todo = await new TodoMethods().createNewTodo(JSON.parse(todoData))
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(todo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error}))
        }
        
    }  else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="PATCH") {
        try {
            const id = request.url.split('/')[2];

            const updatedTodo = await new TodoMethods().updateTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            console.log(updatedTodo)
            response.end(JSON.stringify(updatedTodo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
        
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="PUT") {
        try {
            const id = request.url.split('/')[2];
            const updatedTodo = await new TodoMethods().updateTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(updatedTodo))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
        
        
    } else if (request.url.match(/\/todos\/([0-9]+)/) && request.method ==="DELETE") {
        try {
            const id = request.url.split('/')[2];
            const message = await new TodoMethods().deleteTodo(id)
            response.writeHead(200, {"Content-Type": "application/json"});
            response.end(JSON.stringify(message))
        } catch (error) {
            response.writeHead(404, {"Content-Type": "application/json"})
            response.end(JSON.stringify({message: error.message}))
        }
    } else {
        response.writeHead(404, {"Content-Type": "application/json"})
        response.end(JSON.stringify({message: "URL is not matched"}))
    }
})

function getReqData(request) {
    return new Promise((resolve, reject) => {
        try {
            let body = "";
            request.on("data", (chunk) => {
                body += chunk.toString();
            });
            request.on("end", () => {
                resolve(body);
            });
        } catch (error) {
            reject(error);
        }
    });
}