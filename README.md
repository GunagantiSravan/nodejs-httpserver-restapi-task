
**create a simple Node.js server (without express) for Todo app following REST conventions, with few API endpoints**
```
server should expose these API's

GET	/todos - get all todos
GET	/todos/:id - get a todo with a ID
POST /todos - add a todo
PUT	/todos/:id - update a todo
PATCH /todos/:id - update a todo
DELETE /todos/:id - delete a todo```
```
